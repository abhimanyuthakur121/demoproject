package com.example.rajput123.thereddoor.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.example.rajput123.thereddoor.Activities.YourAppointmentActivity;
import com.example.rajput123.thereddoor.Entities.AppointmentDetails;
import com.example.rajput123.thereddoor.R;

/**
 * Created by rajput123 on 30-10-2015.
 */


public class AppointmentAdapter extends BaseAdapter implements View.OnClickListener{
private ArrayList<AppointmentDetails> appointmentDetailsArrayList;
    AppointmentDetails apt;
    private Context context;
    TextView tvName,tvService,tvDuration,tvLocation,tvTime,tvTimeleft;
    ImageView imageView;
    ImageView ivPic;

    public AppointmentAdapter(Context mContext, ArrayList<AppointmentDetails> appointmentDetailsArrayList) {
        this.appointmentDetailsArrayList = appointmentDetailsArrayList;
        this.context = mContext;
    }
    @Override
    public int getCount() {
        return appointmentDetailsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return appointmentDetailsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = LayoutInflater.from(context);
        convertView = li.inflate(R.layout.appointment, parent, false);
        tvName = (TextView) convertView.findViewById(R.id.name);
        tvService = (TextView) convertView.findViewById(R.id.customerService);
        tvDuration = (TextView) convertView.findViewById(R.id.timed);
        tvLocation = (TextView) convertView.findViewById(R.id.location);
        tvTime = (TextView) convertView.findViewById(R.id.dateBar3);
        tvTimeleft = (TextView) convertView.findViewById(R.id.dateBar2);
        ivPic = (ImageView) convertView.findViewById(R.id.image);
        imageView=(ImageView)convertView.findViewById(R.id.map);
        AppointmentDetails appointmentDetails = appointmentDetailsArrayList.get(position);
        tvName.setText(appointmentDetails.getName());
        tvService.setText(appointmentDetails.getService());
        tvDuration.setText(appointmentDetails.getDuration());
        tvLocation.setText(appointmentDetails.getLocation());
        tvTime.setText(appointmentDetails.getAppointmentDate());
        tvTimeleft.setText(appointmentDetails.getInvitationDate());
        ivPic.setImageResource(appointmentDetails.getPic());
        if (YourAppointmentActivity.seeInvitationState)
            tvTimeleft.setVisibility(View.VISIBLE);
        else
            tvTimeleft.setVisibility(View.GONE);
        return convertView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.map:
                Intent intent=new Intent();
                intent.putExtra(" current location", apt.getLocation());
                context.startActivity(intent);
                break;

        }
    }
}

