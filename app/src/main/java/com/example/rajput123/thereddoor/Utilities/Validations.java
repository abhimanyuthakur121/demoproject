package com.example.rajput123.thereddoor.Utilities;

import com.example.rajput123.thereddoor.Constants.Conventions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rajput123 on 06-11-2015.
 */
public class Validations implements Conventions {

    public static boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean validatePassword(String password) {
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean validateFirstName(String firstName) {
        Pattern pattern = Pattern.compile(FIRST_NAME_PATTERN);
        Matcher matcher = pattern.matcher(firstName);
        return matcher.matches();
    }

    public static boolean validateLastName(String lastName) {
        Pattern pattern = Pattern.compile(LAST_NAME_PATTERN);
        Matcher matcher = pattern.matcher(lastName);
        return matcher.matches();
    }
    public boolean Validatephone(String phone) {
        Pattern pattern = Pattern.compile(PHONE_CONSTRAINT);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    public boolean ValidateconfirmPassword(String pass, String confirmPass) {
        return pass.equals(confirmPass);
    }
    public boolean validateempId(String empId) {
        if (true)
            return true;
        else
            return false;
    }
    public boolean validateaboutMe(String aboutMe) {
        int count = aboutMe.length();
        if (count > ABOUT_ME_MIN_LENGTH && count < ABOUT_ME_MAX_LENGTH)
            return true;
        else
            return false;
    }
}

