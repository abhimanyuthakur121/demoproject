package com.example.rajput123.thereddoor.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rajput123.thereddoor.Constants.Conventions;
import com.example.rajput123.thereddoor.Models.LoginResponse;
import com.example.rajput123.thereddoor.R;
import com.example.rajput123.thereddoor.RetroFit.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AboutMEActivity extends AppCompatActivity implements View.OnClickListener,Conventions {
    TextView header3;
    Button save;
    ImageButton back;
    EditText ed1;
    TextView mtextwacher;
    String aboutmetext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);
        header3 = (TextView) findViewById(R.id.textView);
        header3.setText("ABOUT ME");
        save = (Button) findViewById(R.id.save);
        ed1 = (EditText) findViewById(R.id.ed1);
        back=(ImageButton)findViewById(R.id.back2);
        mtextwacher=(TextView)findViewById(R.id.tv0);
        ed1.addTextChangedListener(mtextwatcher);
       // save.setOnClickListener(this);
        back.setOnClickListener(this);
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent back= new Intent(AboutMEActivity.this,SignUpActivity.class);
//                startActivity(back);
//            }
//        });
        save.setOnClickListener(this);
    }
    private final TextWatcher mtextwatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
mtextwacher.setText(String.valueOf(s.length()+"|"+"140"));
        }
    };


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.save:
                aboutmetext = ed1.getText().toString();
                if (!aboutmetext.isEmpty()) {
                    retrofitCallAboutMe();
                } else {
                    Toast.makeText(this, "Information is empty", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.back2:
                intent(SignInActivity.class);
                break;

        }
    }
    private void intent(Class other)
    {
        Intent next = new Intent(AboutMEActivity.this, other);
        startActivity(next);
        finish();
    }
    public void retrofitCallAboutMe() {
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(AboutMEActivity.this);
        String token=sharedPreferences.getString(ACCESS_TOKEN,"");
        new RestClient().getService().putAboutMe("Bearer " + token, ed1.getText().toString(), new Callback<LoginResponse>() {
            @Override
            public void success(LoginResponse registerResponse, Response response) {

                Intent goToAddress = new Intent(AboutMEActivity.this, AddressActivity.class);
                startActivity(goToAddress);
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(AboutMEActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
