package com.example.rajput123.thereddoor.Models;

/**
 * Created by rajput123 on 05-11-2015.
 */
public class RegisterResponse {
    String  statusCode;
    String  error;
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
