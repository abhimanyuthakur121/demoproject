package com.example.rajput123.thereddoor.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.rajput123.thereddoor.R;
import com.example.rajput123.thereddoor.Entities.RegionData;

import java.util.ArrayList;

/**
 * Created by rajput123 on 06-11-2015.
 */
public class RegionSpinnerAdapter extends BaseAdapter {




        Context context;
        ArrayList<RegionData> regionNameArrayList;

        public RegionSpinnerAdapter(Context context, ArrayList<RegionData> regionNameArrayList) {
            this.context = context;
            this.regionNameArrayList = regionNameArrayList;
        }

        @Override
        public int getCount() {
            return regionNameArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return regionNameArrayList.get(position).getRegionName();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = View.inflate(context, android.R.layout.simple_list_item_1, null);
            TextView textView = (TextView)convertView.findViewById(android.R.id.text1);
            textView.setText(regionNameArrayList.get(position).getRegionName().toString());
            return convertView;
        }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view;
        if (position == 0) {
            view = inflater.inflate(R.layout.hidden_layout, parent, false); // Hide first row
        } else {
            view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText(regionNameArrayList.get(position).getRegionName());
            textView.setTextColor(Color.BLACK);
        }

        return view;
    }

}

