package com.example.rajput123.thereddoor.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rajput123.thereddoor.Constants.Conventions;
import com.example.rajput123.thereddoor.R;

public class InvitationActivity extends AppCompatActivity  implements View.OnClickListener,Conventions {
    TextView header5;
    Button accept,decline;
    ImageView map;
    ImageButton back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation);
        header5=(TextView)findViewById(R.id.textView);
        header5.setText("INVITATION");
        map=(ImageView)findViewById(R.id.map);
        accept=(Button)findViewById(R.id.accept);
        decline=(Button)findViewById(R.id.decline);
        back=(ImageButton)findViewById(R.id.back2);
        map.setVisibility(View.GONE);
        accept.setOnClickListener(this);
        decline.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.back2:
                intent(YourAppointmentActivity.class);
                break;
            case R.id.accept:
                break;
            case R.id.decline:
                break;
        }

    }
    private void intent(Class next)
    {
        Intent back = new Intent(InvitationActivity.this, next);
        startActivity(back);
        finish();
    }
}
