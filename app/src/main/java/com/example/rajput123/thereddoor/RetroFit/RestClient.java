package com.example.rajput123.thereddoor.RetroFit;

import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by rajput123 on 03-11-2015.
 */
public class RestClient {

        APIservices service;
        final String BASE_URL = "http://reddoorspa.clicklabs.in:8000";

        public RestClient() {
            OkHttpClient okHttpClient = new OkHttpClient();
            RestAdapter.Builder builder = new RestAdapter.Builder()
                    .setEndpoint(BASE_URL)
                    .setClient(new OkClient(okHttpClient))
                    .setLogLevel(RestAdapter.LogLevel.FULL);

            RestAdapter restAdapter = builder.build();
            service = restAdapter.create(APIservices.class);

        }

        public APIservices getService() {
            return service;
        }
    }


