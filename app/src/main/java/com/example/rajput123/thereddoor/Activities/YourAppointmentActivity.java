package com.example.rajput123.thereddoor.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rajput123.thereddoor.Adapter.AppointmentAdapter;
import com.example.rajput123.thereddoor.Entities.AppointmentDetails;
import com.example.rajput123.thereddoor.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class YourAppointmentActivity extends AppCompatActivity implements View.OnClickListener {
    TextView header5;
    ImageButton imageButton;
    ImageButton menu,menuPressed;
    ArrayList<AppointmentDetails> appointmentDetailsArrayList=new ArrayList<>();
    private ListView mDrawerList,list;
    AppointmentAdapter listAppointmentAdapter;
    ArrayAdapter<String> mAdapter;
    private DrawerLayout mDrawerLayout;
    Button scheduled;
    Button invitations;
    SharedPreferences sharedPreferences;
    RelativeLayout navigationlist;

    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    public static  boolean seeInvitationState;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_appointment);
        mDrawerList = (ListView) findViewById(R.id.navigationlist);
        header5=(TextView)findViewById(R.id.textView);
        imageButton=(ImageButton)findViewById(R.id.back2);
        scheduled=(Button)findViewById(R.id.scheduled);
        navigationlist =(RelativeLayout)findViewById(R.id.navigationlayout);
        invitations=(Button)findViewById(R.id.invitations);
        imageButton.setVisibility(View.INVISIBLE);
        header5.setText("YOUR APPOINTMENT");
        list=(ListView)findViewById(R.id.list);
        listAppointmentAdapter =new AppointmentAdapter(this,appointmentDetailsArrayList);
        list.setAdapter(listAppointmentAdapter);
        listAppointmentAdapter.notifyDataSetChanged();
        createList();
        menu=(ImageButton)findViewById(R.id.menu);
        menuPressed=(ImageButton)findViewById(R.id.menuPressed);
        menu.setOnClickListener(this);
        menuPressed.setOnClickListener(this);
        scheduled.setOnClickListener(this);
        invitations.setOnClickListener(this);
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);
        navigationDrawer();

    }
    private void setupDrawer() {
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }
    private void addDrawerItems() {
        String[] osArray = {"NAME", "HOME","MY ACCOUNT","OPEN APPOINTMENT","ABOUT ME","BOOKING HISTORY","RAISE ALARM","LOGOUT",};
        mAdapter = new ArrayAdapter<String>(YourAppointmentActivity.this, android.R.layout.simple_list_item_1, osArray);

    }
    private void navigationDrawer()
    {
        addDrawerItems();
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> mAdapterView, View mDrawerList, int position, long id) {
                switch (position) {
                    case 0:
                        Toast.makeText(YourAppointmentActivity.this, "NAME", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Intent b = new Intent(YourAppointmentActivity.this, MainActivity.class);
                        startActivity(b);
                        break;
                    case 7:
                    Intent a = new Intent(YourAppointmentActivity.this, SignInActivity.class);
                    startActivity(a);
                    break;
                }
            }
        });
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                mDrawerToggle.syncState();
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }
        };
        setupDrawer();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.menu:
                menu.setVisibility(View.INVISIBLE);
                menuPressed.setVisibility(View.VISIBLE);
                mDrawerLayout.openDrawer(navigationlist);
                break;
            case R.id.menuPressed:
                menu.setVisibility(View.VISIBLE);
                menuPressed.setVisibility(View.INVISIBLE);
                mDrawerLayout.closeDrawer(navigationlist);
                break;
            case R.id.scheduled:
                scheduled.setBackgroundResource(R.mipmap.small_btn_pressed);
                invitations.setBackgroundResource(R.mipmap.small_btn_normal);
                scheduled.setTextColor(Color.WHITE);
                invitations.setTextColor(Color.BLACK);
                seeInvitationState=false;
                listAppointmentAdapter.notifyDataSetChanged();
                break;
            case R.id.invitations:
                invitations.setBackgroundResource(R.mipmap.small_btn_pressed);
                scheduled.setBackgroundResource(R.mipmap.small_btn_normal);
                scheduled.setTextColor(Color.BLACK);
                invitations.setTextColor(Color.WHITE);
                seeInvitationState=true;
                listAppointmentAdapter.notifyDataSetChanged();
                break;
        }
    }



    private void createList()
    {
        AppointmentDetails appointment;
        for(int i=0;i<10;i++)
        {
            SimpleDateFormat form=new SimpleDateFormat("h:mm a,MMM dd,yyyy");
            appointment=new AppointmentDetails();
            appointment.setAppointmentDate(form.format(new Date()));
            appointment.setAppointmentDate(form.format(new Date()));
            appointment.setPic(R.mipmap.small_placeholder);
            appointment.setName("Testing :- " + i);
            appointment.setDuration("Temp Duration " + i);
            appointment.setService("Temp Service " + i);
            appointment.setCustomerNotes("Empty Notes " + i);
            appointment.setInvitationDate("temp invite " + i);
            appointment.setLocation("Chandigarh,India ");
            appointmentDetailsArrayList.add(appointment);
        }

    }

}
