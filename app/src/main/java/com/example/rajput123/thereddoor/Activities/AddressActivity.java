package com.example.rajput123.thereddoor.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rajput123.thereddoor.R;
import com.example.rajput123.thereddoor.RetroFit.MyResponse;
import com.example.rajput123.thereddoor.RetroFit.RestClient;
import com.example.rajput123.thereddoor.Constants.Conventions;
import com.example.rajput123.thereddoor.Utilities.ConvertLocationToLatlong;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AddressActivity extends AppCompatActivity implements View.OnClickListener,Conventions {
    TextView header4;
    Button save1;
    ImageButton back;
    TextView mapclick;
    EditText editStreet, editCity, editSuite, editZip, editState;
    String street, suite, city, state, zip;
    double lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        header4 = (TextView) findViewById(R.id.textView);
        header4.setText("ADDRESS");
        save1 = (Button) findViewById(R.id.save1);
        back = (ImageButton) findViewById(R.id.back2);
        editStreet = (EditText) findViewById(R.id.streetedit);
        editCity = (EditText) findViewById(R.id.cityedit);
        editSuite = (EditText) findViewById(R.id.apartedit);
        editZip = (EditText) findViewById(R.id.zipedit);
        editState = (EditText) findViewById(R.id.stateedit1);
        back.setOnClickListener(this);
        save1.setOnClickListener(this);
        mapclick = (TextView) findViewById(R.id.mapview);
        mapclick.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back2:
                super.onBackPressed();
                break;
            case R.id.save1:
                validate();
                save1.setBackgroundResource(R.mipmap.big_btn_pressed);
                save1.setTextColor(Color.WHITE);
                break;
            case R.id.mapview:
                Intent intent = new Intent(AddressActivity.this, MapsActivity.class);
                startActivityForResult(intent, ADDRESS_REQUEST_CODE);
                break;
        }
    }

    public void validate() {

        zip = editZip.getText().toString();
        city = editCity.getText().toString();
        street = editStreet.getText().toString();
        state = editState.getText().toString();
        suite = editSuite.getText().toString();
        if (zip != null && !zip.isEmpty()
                && city != null && !city.isEmpty()
                && street != null && !street.isEmpty()
                && state != null && !state.isEmpty()
                && suite != null && !suite.isEmpty()) {
            if (lat == 0.0 || lng == 0.0) {
                String agentAddress = suite + " " + street + " " + city + " " + state;
                int status = isGooglePlayServicesAvailable();
                if (status != ConnectionResult.SUCCESS) {
                    Toast.makeText(AddressActivity.this, "MSG_NO_INTERNET_CONNECTION", Toast.LENGTH_SHORT).show();
                } else {
                    ConvertLocationToLatlong convertLocationToLatLng = new ConvertLocationToLatlong();
                    convertLocationToLatLng.convertToAddressDetails(agentAddress, getApplicationContext(), new LatLngHandler());
                }
            } else {
                callRest();
            }
        } else
            Toast.makeText(AddressActivity.this, NULL_VALUE, Toast.LENGTH_SHORT).show();
    }

    private void intent(Class next) {
        Intent back = new Intent(AddressActivity.this, next);
        startActivity(back);
        finish();
    }

    public void callRest() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(AddressActivity.this);
        String token = sharedPreferences.getString(ACCESS_TOKEN, "");
        new RestClient().getService().postAddress("Bearer " + token, street, suite, city, state, zip, lat, lng
                , new Callback<MyResponse>() {
            @Override
            public void success(MyResponse apiUserResponse, Response response) {
                startActivity(new Intent(AddressActivity.this, YourAppointmentActivity.class));
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(AddressActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private int isGooglePlayServicesAvailable() {
        //checking the google services(Internet Connection)
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        return status;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == ADDRESS_REQUEST_CODE) && (resultCode == RESULT_OK)) ;
        String agentAddress = data.getStringExtra("address");
        int status = isGooglePlayServicesAvailable();
        if (agentAddress != null) {
            if (status == ConnectionResult.SUCCESS) {
                ConvertLocationToLatlong convertLocationToLatLng = new ConvertLocationToLatlong();
                convertLocationToLatLng.convertToAddressDetails(agentAddress, getApplicationContext(), new AddressHandler());
            } else
                Toast.makeText(AddressActivity.this, "MSG_NO_INTERNET_CONNECTION", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(AddressActivity.this, "NO_ADDRESS_RECIEVED", Toast.LENGTH_SHORT).show();
    }

    public class AddressHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            switch (msg.what) {
                case 1:
                    String city = bundle.getString(CITY), suite = bundle.getString(SUITE), street = bundle.getString(STREET), state = bundle.getString(STATE), zip = bundle.getString(ZIP);
                    if (city != null)
                        editCity.setText(city);
                    if (zip != null)
                        editZip.setText(zip);
                    if (state != null)
                        editState.setText(state);
                    if (street != null)
                        editStreet.setText(street);
                    if (suite != null)
                        editSuite.setText(suite);
                    double[] latLong;
                    latLong = bundle.getDoubleArray(LAT_LONG_BUNDLE);
                    lat = latLong[0];
                    lng = latLong[1];
                    break;
                case 0:
                default:
                    Toast.makeText(AddressActivity.this, "MSG_ADDRESS_NOT_FOUND", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // to find only LatLong
    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            double[] latLong = bundle.getDoubleArray(LAT_LONG_BUNDLE);

            switch (msg.what) {
                case 1:
                    lat = latLong[0];
                    lng = latLong[1];
                    callRest();
                    break;
                case 0:
                default:
                    Toast.makeText(AddressActivity.this, "MSG_ADDRESS_NOT_FOUND", Toast.LENGTH_SHORT).show();
            }
        }
    }
}