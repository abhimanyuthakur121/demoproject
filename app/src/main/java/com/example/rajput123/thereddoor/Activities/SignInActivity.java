package com.example.rajput123.thereddoor.Activities;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rajput123.thereddoor.Models.LoginResponse;
import com.example.rajput123.thereddoor.R;
import com.example.rajput123.thereddoor.Models.RegisterResponse;
import com.example.rajput123.thereddoor.RetroFit.RestClient;
import com.example.rajput123.thereddoor.Constants.Conventions;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SignInActivity extends AppCompatActivity implements View.OnClickListener,Conventions{
TextView textView;
    TextView header1;
    Button signin;
    ImageButton back;
    EditText email,password;
    String sEmail,sPassword;
    Button forgot;
    ProgressDialog progressDialog;
//    String DEVICE_TYPE ="ANDROID";
//    String DEVICE_TOKEN ="d8oSdG-O3JE:APA91bEA2uP7-LJAQiFBNWaZ2xS7YbeXF8zbZq7MY7uiaJY5vSz2buTr2er_yx8QNNs5DV3xT7B0ypPHKDkHs_gz4tzitEanuHr7qS60y_X-BrlfD4EtIGf0Wn6Trv9bNDrOxRPermoC";
//



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        header1=(TextView)findViewById(R.id.textView);
        header1.setText("SIGN IN");
        email=(EditText)findViewById(R.id.email);
        password=(EditText)findViewById(R.id.edit_password);
        signin=(Button)findViewById(R.id.sign);
        back=(ImageButton)findViewById(R.id.back2);
        back.setOnClickListener(this);
        signin.setOnClickListener(this);

        /*signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sign = new Intent(SignInActivity.this, YourAppointmentActivity.class);
                startActivity(sign);
                //signin.setBackgroundResource(R.mipmap.big_btn_pressed);

            }

        });*/


        TextView tv3 = (TextView)findViewById(R.id.tv3);
        tv3.setOnClickListener(this);

        textView = (TextView)findViewById(R.id.textView);
        Spannable wordtoSpan = new SpannableString("Not Registered Yet?SIGN UP here");
        Spannable wordtoSpan1 = new SpannableString("SIGN UP");
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#CD2E44")), 19, 26, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#ABACAD")), 0, 19, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#ABACAD")), 26, 31, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new UnderlineSpan(), 19, wordtoSpan.length()-4, 26);
        tv3.setText(wordtoSpan);
        textView.setText("SIGN IN");
    }
    public void getLoginCall() {
        sEmail=email.getText().toString();
        sPassword=password.getText().toString();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignInActivity.this);
        String deviceToken = sharedPreferences.getString(DEVICE_TOKEN, "");
        new RestClient().getService().getLogin(sEmail, sPassword,
                DEVICE_TYPE,
                deviceToken, new Callback<LoginResponse>() {
                    @Override
                    public void success(LoginResponse registerResponse, Response response) {
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignInActivity.this);
                        SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
                        sharedEditor.putString(ACCESS_TOKEN, registerResponse.getData().getToken().toString());
                        sharedEditor.putString(FIRST_NAME,registerResponse.getData().getAgent().getName().getFirstName());
                        sharedEditor.commit();
                        Intent goToAppointments = new Intent(SignInActivity.this, YourAppointmentActivity.class);
                        startActivity(goToAppointments);
                        finish();
//                        Toast.makeText(SignUpActivity.this, "Success" + registerResponse.getStatusCode() + " , " + response.getStatus(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(SignInActivity.this, NO_CONNECTION, Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign:
                getLoginCall();
                progressDialog = new ProgressDialog(SignInActivity.this);
                progressDialog.setMessage("data saving");
                progressDialog.show();
                break;
            case R.id.back2:
                finishAffinity();
                break;
            case R.id.tv3:
                intent(SignUpActivity.class);
                break;
        }
    }
    private void progressDialog() {
        progressDialog = new ProgressDialog(SignInActivity.this);
        progressDialog.setMessage(LOGIN);
        progressDialog.show();
        sEmail = email.getText().toString();
        sPassword = password.getText().toString();
        if (sEmail.isEmpty() || sPassword.isEmpty()) {
            progressDialog.dismiss();
        }

    }
    private void intent(Class other)
    {
        Intent signup = new Intent(SignInActivity.this, other);
        startActivity(signup);
        finish();
    }
}
