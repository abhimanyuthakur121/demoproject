package com.example.rajput123.thereddoor.Utilities;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.rajput123.thereddoor.Constants.Conventions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by rajput123 on 09-11-2015.
 */
public class ConvertLocationToLatlong implements Conventions {
    public static void getLatLong(final String customerLocation, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                double[] latLng = new double[2];
                try {
                    List customerAddress = geocoder.getFromLocationName(customerLocation, 1);
                    if (customerAddress != null && customerAddress.size() > 0) {
                        Address address = (Address) customerAddress.get(0);
                        latLng[0] = address.getLatitude();
                        latLng[1] = address.getLongitude();

                    }
                } catch (IOException e) {
                    Log.e(EXCEPTION, UNABLE_TO_CONNECT, e);
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    message.what = 1;
                    Bundle bundle = new Bundle();
                    bundle.putDoubleArray(LATLONG, latLng);
                    message.setData(bundle);
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }

    public  void convertToAddressDetails(final String locationAddress, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String zip = "", city = "", street = "", state = "", suite = "";
                double[] latLong = new double[2];
                int messageType = 0;
                try {
                    List addressList = geocoder.getFromLocationName(locationAddress, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = (Address) addressList.get(0);
                        zip = address.getPostalCode();
                        city =  address.getLocality();
                        street = address.getFeatureName();
                        state = address.getAdminArea();
                        suite = address.getSubLocality();
                        messageType = 1;
                        latLong[0] = address.getLatitude();
                        latLong[1] = address.getLongitude();
                    } else {
                        messageType = 0;
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Unable to connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.what = messageType;
                    message.setTarget(handler);
                    Bundle bundle = new Bundle();
                    bundle.putString(ZIP, zip);
                    bundle.putString(CITY, city);
                    bundle.putString(STATE, state);
                    bundle.putString(STREET, street);
                    bundle.putString(SUITE, suite);
                    bundle.putDoubleArray(LAT_LONG_BUNDLE, latLong);
                    message.setData(bundle);
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }
}


