package com.example.rajput123.thereddoor.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rajput123.thereddoor.Constants.Conventions;
import com.example.rajput123.thereddoor.R;
import com.example.rajput123.thereddoor.Utilities.ConvertLocationToLatlong;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,GoogleMap.OnMyLocationChangeListener, AdapterView.OnItemClickListener,View.OnClickListener,Conventions {


    ImageButton back;
    GoogleMap map;
    TextView textView;
    Button done;
    double[] latlng;
    Boolean locationSelected = false;
    AutoCompleteTextView autoCompleteTextView;

    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyBnXERuNgEsioHih99F9Lm9Ykt8MlzPGt4";
     // Might be null if Google Play services APK is not available.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        done=(Button)findViewById(R.id.done);
        done.setOnClickListener(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if (status != ConnectionResult.SUCCESS) {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            map = fm.getMap();
            map.setMyLocationEnabled(true);
            map.setOnMyLocationChangeListener(this);
        }

        mapFragment.getMapAsync(this);
      AutoCompleteTextView autoCompView = (AutoCompleteTextView) findViewById(R.id.chooseLocation);
       autoCompView.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.auto_complete_text_view));
        autoCompView.setOnItemClickListener(this);
        back=(ImageButton)findViewById(R.id.back2);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(MapsActivity.this, AddressActivity.class);
                startActivity(back);
            }
        });
        textView = (TextView)findViewById(R.id.textView);
        textView.setText("SELECT LOCATION");

    }
    private void setLocation() {
        map.clear();
        LatLng latLng = new LatLng(latlng[0], latlng[1]);
        map.addMarker(new MarkerOptions().position(latLng).anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.mipmap.your_location_icon)));
        map.animateCamera(CameraUpdateFactory.newLatLng(latLng));
    }
    public boolean googleMapAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
            return true;
        else
            return false;
    }
    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case LAT_LNG_RECEIVED:
                    Bundle bundle = msg.getData();
                    latlng = bundle.getDoubleArray(LAT_LNG_BUNDLE);
                    locationSelected = true;
                    setLocation();
                    break;
                default:
                    Toast.makeText(MapsActivity.this, ADDRESS_NOT_FOUND, Toast.LENGTH_SHORT).show();
            }
        }
    }

   @Override
    public void onMapReady(GoogleMap googleMap) {
   }

    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        String agentAddress = (String) adapterView.getItemAtPosition(position);
        if (!googleMapAvailable()) {
            Toast.makeText(MapsActivity.this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();

        } else {

            ConvertLocationToLatlong convertLocationToLatLng = new ConvertLocationToLatlong();
            convertLocationToLatLng.getLatLong(agentAddress, getApplicationContext(), new LatLngHandler());
        }
        Toast.makeText(this, agentAddress, Toast.LENGTH_SHORT).show();

    }

    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
//            sb.append("&components=country:ind");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            return resultList;
        } catch (IOException e) {
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
        }
        return resultList;
    }
    @Override
    public void onMyLocationChange(Location location) {
        TextView textView1 = (TextView) findViewById(R.id.chooseLocation);
        if(!locationSelected) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            map.animateCamera(CameraUpdateFactory.zoomTo(15));
            map.addMarker(new MarkerOptions().position(latLng).title("Marker"));
            // textView1.setText("Latitude:" + latitude + "Longitude:" + longitude);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                Intent intent = new Intent();
                intent.putExtra(ADDRESS_ID, autoCompleteTextView.getText().toString());
                setResult(RESULT_OK, intent);
                finish();


                break;
        }
        }



    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList = new ArrayList();

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return String.valueOf(resultList.get(index));
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());
                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }

    }

}

