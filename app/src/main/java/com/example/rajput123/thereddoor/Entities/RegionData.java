package com.example.rajput123.thereddoor.Entities;

/**
 * Created by rajput123 on 06-11-2015.
 */
public class RegionData {
    String regionName, regionNickname, zip, id, __v;

    public String getRegionNickname() {
        return regionNickname;
    }

    public void setRegionNickname(String regionNickname) {
        this.regionNickname = regionNickname;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
}

