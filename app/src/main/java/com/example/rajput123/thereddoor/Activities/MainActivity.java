package com.example.rajput123.thereddoor.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.rajput123.thereddoor.R;
import com.example.rajput123.thereddoor.Constants.Conventions;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements Conventions {
    //TextView header;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        new RetrieveTokenTask().execute();

        new Handler().postDelayed(new Runnable() {

            // Using handler with postDelayed called runnable run method

            @Override
            public void run() {
                Intent i = new Intent(MainActivity.this, SignInActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, 2 * 1000); // wait for 2 seconds


    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    private class RetrieveTokenTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String scopes = "GCM";
            String Id = "795639679599";
            String token = null;
            try {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();    //Checking the Internet Connection
                Process p1 = java.lang.Runtime.getRuntime().exec(ADDRESS_SPLASH);
                int returnVal = p1.waitFor();
                boolean reachable = (returnVal == RETURN_VALUE);
                if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting() && reachable) {
                    token = InstanceID.getInstance(MainActivity.this).getToken(Id, scopes);
                } else {
                    Toast.makeText(MainActivity.this, CHECK_CONNECTION, Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return token;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null && !s.isEmpty()) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
                sharedEditor.putString(DEVICE_TOKEN, s.toString());
                sharedEditor.commit();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent mainIntent = new Intent(MainActivity.this, SignInActivity.class);
                        MainActivity.this.startActivity(mainIntent);
                        MainActivity.this.finish();
                    }
                }, SPLASH_DISPLAY_LENGTH);

            } else {
                // Toast.makeText(MainActivity.this, CHECK_CONNECTION, Toast.LENGTH_SHORT).show();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set dialog message
                alertDialogBuilder
                        .setMessage(CHECK_CONNECTION)
                        .setCancelable(false)
                        .setPositiveButton(TRY_AGAIN, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, close
                                // current activity
                                new RetrieveTokenTask().execute();
//                                dialog.cancel();
//                                MainActivity.this.finish();
                            }
                        })
                        .setNegativeButton(EXIT, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                MainActivity.this.finish();
                                dialog.cancel();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        }
    }
}






