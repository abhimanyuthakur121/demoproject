package com.example.rajput123.thereddoor.Models;

import com.example.rajput123.thereddoor.Entities.RegionData;

import java.util.ArrayList;


/**
 * Created by rajput123 on 05-11-2015.
 */
public class RegionResponse {
    String message;
    ArrayList<RegionData> data;

    public ArrayList<RegionData> getData() {
        return data;
    }

    public void setData(ArrayList<RegionData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
