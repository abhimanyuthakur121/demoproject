package com.example.rajput123.thereddoor.RetroFit;

import android.graphics.Region;

import com.example.rajput123.thereddoor.Models.LoginResponse;
import com.example.rajput123.thereddoor.Models.RegisterResponse;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.PartMap;
import retrofit.mime.TypedString;

/**
 * Created by rajput123 on 08-11-2015.
 */
public interface APIservices {
    String GET_COUNTRY_CODE_URL = "/api/agent/region";
    String GET_REGIONS = "/api/agent/region";
    String POST_LOGIN = "/api/agent/login";
    String PUT_LOGOUT = "/api/agent/logout";
    String POST_ADDRESS = "/api/agent/address";

    String POST_REGISTER = "/api/agent/register";
    String PUT_ABOUT_ME = "/api/agent/aboutme";

    @GET(GET_COUNTRY_CODE_URL)
    void getRegions(Callback<MyResponse>callback);


    @FormUrlEncoded
    @POST(POST_LOGIN)
    void getLogin(@Field("emailId") String email,
                  @Field("password") String password,
                  @Field("deviceType") String deviceType,
                  @Field("deviceToken") String deviceToken, Callback<LoginResponse> callback);

    @PUT(PUT_LOGOUT)
    void logout(@Header("authorization") String authorization, Callback<RegisterResponse> callback);

//    @Multipart
//    @POST(POST_REGISTER)
//    void getRegister(@Part("firstName") TypedString firstName,
//                     @Part("lastName") TypedString lastName,
//                     @Part("emailId") TypedString emailId,
//                     @Part("employeeId") TypedString employeeId,
//                     @Part("region") TypedString region,
//                     @Part("password") TypedString password,
//                     @Part("phoneNumber") TypedString phoneNumber,
//                     @Part("deviceType") TypedString deviceType,
//                     @Part("deviceToken") TypedString deviceToken,
//                     @Part("gender") TypedString gender,
//                     Callback<RegisterResponse> callback);

    @Multipart
    @POST(POST_REGISTER)
    void getRegister(@PartMap Map<String, TypedString> params,
                     Callback<LoginResponse> callback);


    @FormUrlEncoded
    @PUT(PUT_ABOUT_ME)
    void putAboutMe(@Header("authorization") String authorization, @Field("aboutMe") String aboutMe, Callback<LoginResponse> callback);

    @FormUrlEncoded
    @POST(POST_ADDRESS)
    void postAddress(@Header("authorization") String authorization,
                     @Field("streetAddress") String streetAddress,
                     @Field("apartment") String apartment,
                     @Field("city") String city,
                     @Field("state") String state,
                     @Field("zip") String zip,
                     @Field("latitude") double latitude,
                     @Field("longitude") double longitude,
                     Callback<MyResponse> callback);


}
