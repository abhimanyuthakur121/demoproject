package com.example.rajput123.thereddoor.Models;

/**
 * Created by rajput123 on 17-11-2015.
 */
public class LoginResponse {
    String statusCode, message;
    LoginValues data;

    public LoginValues getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public class LoginValues {
        String token;
        Agent agent;

        public Agent getAgent() {
            return agent;
        }

        public String getToken() {
            return token;
        }

        public class Agent {
            String _id, emailId, employeeId, region, phoneNumber, gender, accessToken, __v,
                    profilePicURL, aboutMe, registrationDate;

            public String get__v() {
                return __v;
            }

            public String get_id() {
                return _id;
            }

            public String getAboutMe() {
                return aboutMe;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public void setEmployeeId(String employeeId) {
                this.employeeId = employeeId;
            }

            public void setRegion(String region) {
                this.region = region;
            }

            public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public void setAccessToken(String accessToken) {
                this.accessToken = accessToken;
            }

            public void set__v(String __v) {
                this.__v = __v;
            }

            public void setProfilePicURL(String profilePicURL) {
                this.profilePicURL = profilePicURL;
            }

            public void setAboutMe(String aboutMe) {
                this.aboutMe = aboutMe;
            }

            public void setRegistrationDate(String registrationDate) {
                this.registrationDate = registrationDate;
            }

            public void setName(Name name) {
                this.name = name;
            }

            public String getAccessToken() {
                return accessToken;
            }

            public String getEmailId() {
                return emailId;
            }

            public String getEmployeeId() {
                return employeeId;
            }

            public String getGender() {
                return gender;
            }

            public Name getName() {
                return name;
            }

            public String getPhoneNumber() {
                return phoneNumber;
            }

            public String getProfilePicURL() {
                return profilePicURL;
            }

            public String getRegion() {
                return region;
            }

            public String getRegistrationDate() {
                return registrationDate;
            }

            Name name;

            public class Name {
                String firstName, lastName;

                public void setFirstName(String firstName) {
                    this.firstName = firstName;
                }

                public void setLastName(String lastName) {
                    this.lastName = lastName;
                }

                public String getFirstName() {
                    return firstName;
                }

                public String getLastName() {
                    return lastName;
                }
            }
        }
    }

}

