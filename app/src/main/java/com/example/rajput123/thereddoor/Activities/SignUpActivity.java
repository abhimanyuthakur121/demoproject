package com.example.rajput123.thereddoor.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rajput123.thereddoor.Adapter.GenderSpinnerAdapter;
import com.example.rajput123.thereddoor.Adapter.RegionSpinnerAdapter;
import com.example.rajput123.thereddoor.Models.LoginResponse;
import com.example.rajput123.thereddoor.RetroFit.RestClient;
import com.example.rajput123.thereddoor.R;
import com.example.rajput123.thereddoor.RetroFit.MyResponse;
import com.example.rajput123.thereddoor.Entities.RegionData;
import com.example.rajput123.thereddoor.Constants.Conventions;
import com.example.rajput123.thereddoor.Utilities.Validations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;


public class SignUpActivity extends AppCompatActivity  implements View.OnClickListener,Conventions {
    TextView header2;
    TextView tv;
    GenderSpinnerAdapter genderSpinnerAdapter;
    TextView tv1;
    Button signup;
    ImageButton back;
    EditText firstName, lastName, email, mobile, password, confirmPassword, registeredEmployeeId;
   String sFirstName, sLastName, sEmail, sMobile, sPassword, sConfirmPassword, sRegisteredEmployeeId, sRegion, sGender;
    Spinner region, gender;
    ArrayList<RegionData> regions;
    ProgressDialog dialog;
    RegionSpinnerAdapter regionSpinnerAdapter;
    TypedString DEVICE_TYPE = new TypedString("ANDROID");
//    TypedString DEVICE_TOKEN = new TypedString("d8oSdG-O3JE:APA91bEA2uP7-LJAQiFBNWaZ2xS7YbeXF8zbZq7MY7uiaJY5vSz2buTr2er_yx8QNNs5DV3xT7B0ypPHKDkHs_gz4tzitEanuHr7qS60y_X-BrlfD4EtIGf0Wn6Trv9bNDrOxRPermoC");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        dialog = new ProgressDialog(SignUpActivity.this);
        region = (Spinner) findViewById(R.id.regionSpinner);
        gender = (Spinner) findViewById(R.id.genderSpinner);
        genderSpinnerAdapter = new GenderSpinnerAdapter(this, getResources().getStringArray(R.array.spinnerItems));
        gender.setAdapter(genderSpinnerAdapter);
        header2 = (TextView) findViewById(R.id.textView);
        header2.setText("SIGN UP");
        regions = new ArrayList<>();
        RegionData regionHint = new RegionData();
        regionHint.setRegionName("Region");
        regions.add(regionHint);
        defination();
        signup = (Button) findViewById(R.id.signup);
        regionsApiCall();
        regionSpinnerAdapter = new RegionSpinnerAdapter(this, regions);
        region.setAdapter(regionSpinnerAdapter);
        back = (ImageButton) findViewById(R.id.back2);
        tv = (TextView) findViewById(R.id.tv4);
        tv.setOnClickListener(this);

        Spannable wordtoSpan1 = new SpannableString("Already a Member? SIGN IN");
        wordtoSpan1.setSpan(new ForegroundColorSpan(Color.parseColor("#B9252B")), 18, 25, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan1.setSpan(new ForegroundColorSpan(Color.parseColor("#ABACAD")), 0, 16, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan1.setSpan(new UnderlineSpan(), 18, wordtoSpan1.length(), 25);
        tv.setText(wordtoSpan1);
        tv1 = (TextView) findViewById(R.id.tv5);
        Spannable wordtoSpan = new SpannableString("you agree to terms of services and privacy");
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#B9252B")), 13, 42, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#ABACAD")), 0, 12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new UnderlineSpan(), 13, wordtoSpan.length(), 32);
        tv1.setText(wordtoSpan);

        back.setOnClickListener(this);


        signup.setOnClickListener(this);
    }
    public void regionsApiCall() {
        new RestClient().getService().getRegions(new Callback<MyResponse>() {
            @Override
            public void success(MyResponse myResponse, Response response) {
               /* String s = new Gson().toJson(myResponse);
                Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();*/
                regions.addAll(myResponse.getData());
                regionSpinnerAdapter.notifyDataSetChanged();

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(SignUpActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });
    }


    private void defination() {
        firstName = (EditText) findViewById(R.id.firstname);
        lastName = (EditText) findViewById(R.id.lastname);
        email = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.mobileno);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirmpassword);
        registeredEmployeeId = (EditText) findViewById(R.id.empid);
    }






    public void retrofitCall() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this);
        String deviceToken = sharedPreferences.getString(DEVICE_TOKEN, "");
        Map<String, TypedString> params = new HashMap<>();

        params.put("firstName", new TypedString(firstName.getText().toString()));
        params.put("lastName", new TypedString(lastName.getText().toString()));
        params.put("emailId", new TypedString(email.getText().toString()));
        params.put("employeeId", new TypedString(registeredEmployeeId.getText().toString()));
        params.put("password", new TypedString(password.getText().toString()));
        params.put("region", new TypedString(region.toString()));
        params.put("phoneNumber", new TypedString(mobile.getText().toString()));
        params.put("deviceType",DEVICE_TYPE);
        params.put("deviceToken", new TypedString(DEVICE_TOKEN));
        params.put("gender", new TypedString(sGender));

        if(deviceToken!=null) {
            new RestClient().getService().getRegister(params, new Callback<LoginResponse>() {
                        @Override
                        public void success(LoginResponse registerResponse, Response response) {
                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this);
                            SharedPreferences.Editor sharedEditor = sharedPreferences.edit();
                            sharedEditor.putString(ACCESS_TOKEN, registerResponse.getData().getToken().toString());
                            sharedEditor.putString(FIRST_NAME,registerResponse.getData().getAgent().getName().getFirstName());
                            sharedEditor.commit();
                            intent(AboutMEActivity.class);
                            Toast.makeText(SignUpActivity.this, "Success", Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(SignUpActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

        }
        else
            Toast.makeText(SignUpActivity.this,"Device Token Null",Toast.LENGTH_SHORT).show();
    }

    public void validate1() {
        Validations validate=new Validations();
        sFirstName = firstName.getText().toString();
        sLastName = lastName.getText().toString();
        sEmail = email.getText().toString();
        sMobile = mobile.getText().toString();
        sPassword = password.getText().toString();
        sConfirmPassword = confirmPassword.getText().toString();
        sRegisteredEmployeeId = registeredEmployeeId.getText().toString();
        sGender = gender.getSelectedItem().toString();
        sRegion = region.getSelectedItem().toString();
        Boolean validFirstName = validate.validateFirstName(sFirstName);
        Boolean validLastName = validate.validateLastName(sLastName);
        Boolean validEmail = validate.validateEmail(sEmail);
        Boolean validPhone = validate. Validatephone(sMobile);
        Boolean validPassword = validate.validatePassword(sPassword);
        Boolean validEmpId = validate.validateempId(sRegisteredEmployeeId);
        Boolean validConfirmPassword = validate.ValidateconfirmPassword(sPassword, sConfirmPassword);
        if (validFirstName && validLastName && validEmail && validPassword && validConfirmPassword && validEmpId) {
            retrofitCall();

            dialog.setMessage(SAVING);
            dialog.show();
        } else if (!validFirstName)
            Toast.makeText(this, INVALID_FIRST_NAME, Toast.LENGTH_SHORT).show();
        else if (!validLastName)
            Toast.makeText(this, INVALID_LAST_NAME, Toast.LENGTH_SHORT).show();
        else if (!validEmail)
            Toast.makeText(this, INVALID_EMAIL, Toast.LENGTH_SHORT).show();
        else if (!validPassword)
            Toast.makeText(this, INVALID_PASSWORD, Toast.LENGTH_SHORT).show();
        else if (!validConfirmPassword)
            Toast.makeText(this, INVALID_CONFIRM_PASSWORD, Toast.LENGTH_SHORT).show();
        else if (!validPhone)
            Toast.makeText(this, INVALID_PHONE, Toast.LENGTH_SHORT).show();
        else if (!validEmpId)
            Toast.makeText(this, INVALID_EMP_ID, Toast.LENGTH_SHORT).show();


    }
    public void intent(Class other) {
        Intent intent = new Intent(SignUpActivity.this, other);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.signup:
                validate1();

                dialog.dismiss();
                break;
            case R.id.back2:
                intent(SignInActivity.class);
                break;
            case R.id.tv4:
                intent(SignInActivity.class);
                break;
        }
    }

    }






