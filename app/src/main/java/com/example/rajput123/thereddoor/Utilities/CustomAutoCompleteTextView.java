package com.example.rajput123.thereddoor.Utilities;

import android.content.Context;
import android.util.AttributeSet;
import java.util.HashMap;
import android.widget.AutoCompleteTextView;


/**
 * Created by rajput123 on 05-11-2015.
 */
public class CustomAutoCompleteTextView extends AutoCompleteTextView {
    public CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Returns the Place Description corresponding to the selected item
     */
    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        /** Each item in the autocompetetextview suggestion list is a hashmap object */
        HashMap<String, String> hm = (HashMap<String, String>) selectedItem;
        return hm.get("description");
    }
}

