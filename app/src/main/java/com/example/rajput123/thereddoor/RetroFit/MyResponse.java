package com.example.rajput123.thereddoor.RetroFit;

import com.example.rajput123.thereddoor.Entities.RegionData;

import java.util.ArrayList;

/**
 * Created by rajput123 on 05-11-2015.
 */
public class MyResponse {
    String statusCode;
    String message;
    ArrayList<RegionData> data;



        String token;
        Agent agent;


        public Agent getAgent() {
            return agent;
        }

        public String getToken() {
            return token;
        }

        public class Agent {
            String _id, emailId, employeeId, region, phoneNumber, gender, accessToken, __v,
                    profilePicURL, aboutMe, registrationDate;

            public String get__v() {
                return __v;
            }

            public String get_id() {
                return _id;
            }

            public String getAboutMe() {
                return aboutMe;
            }

            public String getAccessToken() {
                return accessToken;
            }

            public String getEmailId() {
                return emailId;
            }

            public String getEmployeeId() {
                return employeeId;
            }

            public String getGender() {
                return gender;
            }

            public Name getName() {
                return name;
            }

            public String getPhoneNumber() {
                return phoneNumber;
            }

            public String getProfilePicURL() {
                return profilePicURL;
            }

            public String getRegion() {
                return region;
            }

            public String getRegistrationDate() {
                return registrationDate;
            }

            Name name;

            public class Name {
                String firstName, lastName;

                public String getFirstName() {
                    return firstName;
                }

                public String getLastName() {
                    return lastName;
                }
            }
        }

    public ArrayList<RegionData> getData() {
        return data;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public void setToken(String token) {
        this.token = token;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }



    public void setData(ArrayList<RegionData> data) {
            this.data = data;
        }
    }
